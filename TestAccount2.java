public class TestAccount2 {
    public static void main(String[] args) {
       Account arrayOfAccounts2[] =new Account[5]; 
       Account.setInterestRate(2);
       double[] amounts = {23,5444,2,345,34};
       String[] names = {"Picard", "Ryker", "Worf", "Troy", "Data"};
       
       for (int i = 0; i < arrayOfAccounts2.length; i++) {
        arrayOfAccounts2[i] = new Account(names[i], amounts[i]);
        System.out.println("The name is " + arrayOfAccounts2[i].getName() + " The balance is " + arrayOfAccounts2[i].getBalance());
        arrayOfAccounts2[i].addInterest();
        System.out.println("The name is " + arrayOfAccounts2[i].getName() + " The new balance is " + arrayOfAccounts2[i].getBalance());

        arrayOfAccounts2[i].withdraw(20);
        arrayOfAccounts2[i].withdraw();
        }

    }
}