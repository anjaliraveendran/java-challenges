
public class SavingsAccount extends Account
{

	public SavingsAccount(String name, double bal)
	{
		super (name,bal);
	}

	@Override
	public void addInterest()
	{
		this.setBalance(getBalance()*1.4);
	}




}