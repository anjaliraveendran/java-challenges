import java.util.HashSet;
import java.util.Iterator;

public class CollectionsTest {
    public static void main(String[] args) {
        HashSet<Account> accounts=new HashSet<Account>();
        
        double[] amounts = {23,5444,2};
        String[] names = {"Picard", "Ryker", "Worf"};
        
        
		for (int i=0; i<3; i++)
		{
			Account current = new Account();
			current.setName(names[i]);
			current.setBalance(amounts[i]);
			accounts.add(current);
        }
        
        Iterator<Account> iter = accounts.iterator();

        while(iter.hasNext()){
            Account nextAcc = iter.next();
            System.out.println("Name is " + nextAcc.getName());
            System.out.println("Balance is " + nextAcc.getBalance());
            nextAcc.addInterest();
            System.out.println("New balance is " + nextAcc.getBalance());
        }
    }
}