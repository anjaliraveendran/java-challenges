
public abstract class Account implements Detailable {
    
    private double balance;
    private String name;
    static double interestRate;

    public double getBalance() {
        return balance;
    }

    public void setBalance(double b) {
        this.balance = b;
    }

    public String getName() {
        return name;
    }

    public void setName(String n) {
        this.name = n;
    }

    public abstract void addInterest();

	public Account(String n, double b) {
        this.balance=b;
        this.name=n;
	}

	public Account() {
        this("Angie", 50);
	}

    public static double getInterestRate() {
        return interestRate;
    }

    public static void setInterestRate(double interestRate) {
        Account.interestRate = interestRate;
    }

    public boolean withdraw(double withdrawAmount){
        if(balance>withdrawAmount){
            balance=balance-withdrawAmount;
            return true; 
        }else{
            return false;
        }
    }
    
    public boolean withdraw(){
        balance=balance-20;
        if(balance>0){
            System.out.println("YES");
            return true;
        }else{
            System.out.println("FALSE");
            return false;
        }
    }

    @Override
    public String getDetails() {
        // TODO Auto-generated method stub
        return "" + balance + " " + name + " " + interestRate;
    }
}