public class TestInheritance {
    public static void main(String[] args) {

        Account[] arrayOfAccounts3 = {
            new SavingsAccount("Fimb", 4),
            new CurrentAccount("Renn", 6)
        };

        for (int i=0; i<arrayOfAccounts3.length; i++)
		{
            System.out.println("Name: " + arrayOfAccounts3[i].getName());
			System.out.println("Balance: " + arrayOfAccounts3[i].getBalance());
			arrayOfAccounts3[i].addInterest();
			System.out.println("Name: " + arrayOfAccounts3[i].getName());
			System.out.println("Balance: " + arrayOfAccounts3[i].getBalance());
		}
    }
}