public class TestAccount {
    public static void main(String[] args) {
        Account myAccount = new Account();
        myAccount.setBalance(50000);
        myAccount.setName("Angie");

        Account.setInterestRate(2);
        System.out.println("The account name is " + myAccount.getName() + " and the balance is " + myAccount.getBalance());
        myAccount.addInterest();
        System.out.println("The account name is " + myAccount.getName() + " and the balance is " + myAccount.getBalance());

        Account arrayOfAccounts[];
        arrayOfAccounts = new Account[5];

        double balanceAmounts[] = {55,345,34,75,369};
        String accountNames[] = {"Kat", "Ana","Mar","Umar","Ninad"};

       for (int i = 0; i < arrayOfAccounts.length; i++) {
            arrayOfAccounts[i]= new Account();
            arrayOfAccounts[i].setName(accountNames[i]);
            arrayOfAccounts[i].setBalance(balanceAmounts[i]);
            System.out.println(arrayOfAccounts[i].getName() + " " + arrayOfAccounts[i].getBalance());
           
            arrayOfAccounts[i].addInterest();
            System.out.println(arrayOfAccounts[i].getName() + " " + arrayOfAccounts[i].getBalance());
       }



    }
}