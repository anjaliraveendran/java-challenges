package com.citi.training;
/**
 * This is random code. nothing important.
 */
public class HelloWorld {
    public static void main(String[] args) {
      //CHAPTER 3 QUESTION 1,2,3
        String make = "Skoda";
        String model = "Octavia 530D";
        double engineSize = 5.0; 
        byte gear = 3;
        //CHAPTER 3 QUESTION 4, 5
        short speed = (short)(gear * 20);
        int revs = speed*gear;


        System.out.println("The make is" + " " + make);
        System.out.println("The model is" + " " + model);
        System.out.println("The engine size is" + " " +engineSize);
        //CHAPTER 3 QUESTION 6
        System.out.println("The speed is" + " " + speed);
        System.out.println("The revs is" + " " + revs);
        System.out.println("The gear is" + " " + gear);

        //CHAPTER 4 QUESTION 1
        if (engineSize<=1.3){
            System.out.println("The car is a weak car");
        }else{
            System.out.println("The car is powerful");
        }

        //CHAPTER 4 QUESTION 2
        if(gear==5){
            System.out.println("The speed should be over 45mph.");
        } else if (gear==4){
            System.out.println("The speed should be over 30mph.");
        } else if (gear==3){
            System.out.println("The speed should be over 25mph.");
        } else if (gear==2){
            System.out.println("The speed should be over 15mph.");
        } else if(gear ==1){
            System.out.println("The speed should be under 10mph.");
        } else{
            System.out.println("Gear out of range");
        }

        //CHAPTER 4 QUESTION 3
        for (int i = 1900 ; i < 2000 ; i++) {
            if (i%4==0){
                System.out.println(i + "is a leap year");
        }
    }

    //CHAPTER 4 QUESTION 4
    int count = 0;
    for (int i=1900; i<=2000; i++)
    {
      if ((i%4)==0)
      {
        count++;
        System.out.println(i + " is a leapyear.");
      }
      if (count == 5)
        break;
    }


    
    switch (gear)
    {
      case -1:
        System.out.print("Backwards");
        break;
      case 0:
        System.out.print("0mph");
        break;
      case 1:
        System.out.print("<10mph");
        break;
      case 2:
        System.out.print("<25mph");
        break;
      case 3:
        System.out.print("<35mph");
        break;
      case 4:
        System.out.print("<50mph");
        break;
      case 5:
        System.out.print(">50mph");
        break;
        default:
        System.out.println("NONE");
     }

}
}



