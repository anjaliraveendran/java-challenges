package com.citi.training;

public class HelloWorldOld {
    public static void main(String[] args) {
        Car car1 = new Car();
        car1.setMake("Ford"); //error cus make property is private
        
        Car car2 = car1;
        car2.setMake("Renault");

        System.out.println(car2.getMake());

        car2.accelerate();
        System.out.println(car2.getSpeed());
    }
}