public class HomeInsurance implements Detailable {
    private double premium;
    private double excess;
    private double amtInsured;

    public HomeInsurance(double premium, double excess, double amtInsured) {
        this.premium = premium;
        this.excess = excess;
        this.amtInsured = amtInsured;
    }

    @Override
    public String getDetails() {
        // TODO Auto-generated method stub
        return "" + premium + " " + excess + " " + amtInsured;
    }
    
}